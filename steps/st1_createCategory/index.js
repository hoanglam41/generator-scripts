const argv = require('minimist')(process.argv.slice(2));
const csv = require('csvtojson');
fs = require('fs');
const Utils = require('../../libs/Utils');

(async function () {
  const ids = [];

  const categories = await csv().fromFile(argv.file || 'data/Category.csv');
  console.log(categories);

  for (const categ of categories) {
    try {
      const res = await Utils.axios.SB_instance.post('/api/v1/services/categories', {...categ, active: !!categ.active});
      console.log(res.data.data.id);

      ids.push({
        id: res.data.data.id,
        __link_file: categ.__link_file,
        n: categ.n
      });
      console.log("🚀 ~ file: index.js ~ line 22 ~ categ", categ)
    } catch (err) {
      console.log(err.response.data)
    }
  }

  const blobString = await Utils.makeCSV(ids, {id: "id", __link_file: "__link_file", n: "n"});
  await fs.writeFileSync('data/Category_ids.csv', blobString, 'utf8');

  console.log('step 1. DONE')
})();
