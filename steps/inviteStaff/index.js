const argv = require('minimist')(process.argv.slice(2));
const csv = require('csvtojson');
fs = require('fs');
const Utils = require('../../libs/Utils');

(async function () {
  const ids = [];

  let inviteStaffs = await csv().fromFile('data/nailistStaff.csv');
  const staff_ids_list = await csv().fromFile('data/staff_ids.csv');
  const staff_ids = staff_ids_list.map((st) => st.id);
  inviteStaffs = argv.n && inviteStaffs.slice(0, argv.n);

  for (const [index, staff] of inviteStaffs.entries()) {
    try {
      const res = await Utils.axios.SB_instance.post(`/api/v1/apps/085094953333804/staff/${staff_ids[index]}/invitation`, {identity: staff.username});
      console.log(res.data.data.id);
      ids.push({
        sourceId: staff.id,
        staffId: staff_ids[index]
      });
    } catch (err) {
      console.log(err.response.data)
    }
  }

  const blobString = await Utils.makeCSV(ids, {
    sourceId: "sourceId",
    staffId: "staffId"
  });
  await fs.writeFileSync('data/staff_invited.csv', blobString, 'utf8');

  console.log('step 5. DONE')
})();
