const argv = require('minimist')(process.argv.slice(2));
const csv = require('csvtojson');
fs = require('fs');
const Utils = require('../../libs/Utils');

(async function () {
  const ids = [];

  const services = await csv().fromFile('data/Service_ids.csv');
  const staffs = await csv().fromFile('data/staff_ids.csv');
  const serviceIds = services.map((sv) => sv.id);
  const staff_ids = staffs.map((st) => st.id);
  console.log('serviceIds', serviceIds);
  console.log('staff id:', staff_ids);
  for (const id of staff_ids) {
    try {
      const res = await Utils.axios.SB_instance.post(`/api/v1/staff/${id}/services`, {services: serviceIds});
      console.log(res.data);
    } catch (err) {
      console.log(err.response.data)
    }
  }

  console.log('step 4. DONE')
})();
