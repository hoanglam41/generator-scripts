const PATH = 'steps/generateBooking/';
const argv = require('minimist')(process.argv.slice(2));
const axios = require('axios');
const _ = require('lodash');
const moment = require('moment');

// generate full slot schedule
const fullDaySlots = [];
for (let hour = 0; hour < 24; hour++) {
  fullDaySlots.push(hour * 100, hour * 100 + 30);
}

let i = 0;
const numBooking = argv.n || 100;
const nailistId = argv.nailistId || 'ahcC4Gvee8';
const customerId = argv.customerId || '5eCU8Imiwq';
const urlServer = argv.urlServer || process.env.NAILIE_API || 'https://nailie.scrum-dev.com';
const withinDays = argv.withinDays || 10

console.log(argv);

if (numBooking < withinDays*fullDaySlots.length) {
  const data = JSON.stringify({"userId":nailistId});

  const configGetMenu = {
    method: 'post',
    url: urlServer + '/api/functions/getMenusBookingByUser',
    headers: {
      'Content-Type': 'application/json',
      'x-parse-application-id': 'gLnhPy54ASzHkD7xz0a33Rn05yfAOA6D',
      'x-parse-javascript-key': 'PWd5j6R75Ed75kP5TxfOBzdNLOJnofkS'
    },
    data : data
  };

  axios(configGetMenu)
    .then(async function (response) {
      const listMenu = _.map(response.data.result[0].menus, (menu) => {
        return menu.objectId;
      })

      let errCount = 0;
      while (i < numBooking & errCount < 100) {
        const slot = fullDaySlots[Math.floor(Math.random() * (fullDaySlots.length))];
        const randomDay = moment().add(Math.floor(Math.random() * 30), 'days').format("YYYY-MM-DD");
        const randomMenuId = listMenu[Math.floor(Math.random() * listMenu.length)]

        const dataCreateBooking = JSON.stringify({
          "phonetic": "はさら ややら",
          "phone": "05555555555",
          "fullName": "柳田 羅皿",
          "cancelPolicy": "HARD",
          "cardId": "card_1HTla4GCqQxJ3Fy5DDvHH4Yd",
          "paymentMethod": "ONLINE",
          "bookingDate": randomDay,
          "menuBookings": [{"menuId": randomMenuId}],
          "slot": slot,
          "nailistId": nailistId,
          "customerId": customerId,
          "point": 0
        });

        console.log('==============data', dataCreateBooking);

        let configRequestBooking = {
          method: 'post',
          url: urlServer + '/CB/test/createBooking',
          headers: {
            'Content-Type': 'application/json'
          },
          data: dataCreateBooking
        };

        await axios(configRequestBooking)
          .then(function (response) {
            console.log("Successfully created booking");
            i++;
            errCount = 0;
          })
          .catch(function (error) {
            console.log("Failure to create booking");
            console.log(error);
            errCount++
          });
      }
    })
    .catch(function (error) {
      console.log(error);
    });
} else {
  console.log('Not enough slot')
}
