const argv = require('minimist')(process.argv.slice(2));
const generateJapName = require("japanese-name-generator");
const Utils = require('../libs/Utils');
fs = require('fs');

const fileStaff = require(`../data/S${argv.i || 'staff.json'}`).staffs[0];

const CSVStruct = {
    "name": "name",
    "email": "email",
    "phoneNumber": "phoneNumber",
    "birthday": "birthday",
    "gender": "gender",
    "postCode": "postCode",
    "prefecture": "prefecture",
    "cityOrTown": "cityOrTown",
    "address": "address"
}

const n = argv.n || 15;
const outData = [];
for (let i = 0; i < n; i++) {
    let japanesePerson = generateJapName();
    let phone = Utils.genPhoneNum();
    
    outData.push({
        "name": japanesePerson.firstName,
        "email": japanesePerson.firstName + phone.slice(6) + '@yopmail.com',
        "phoneNumber": phone,
        "birthday": fileStaff["birthday"],
        "gender": fileStaff["gender"],
        "postCode": fileStaff["postCode"],
        "prefecture": fileStaff["prefecture"],
        "cityOrTown": fileStaff["cityOrTown"],
        "address": fileStaff["address"]
    })
}

if (argv.o) {
    const blobString = Utils.makeCSV(outData, CSVStruct);
    console.log("🚀 ~ file: generateStaffCSV.js ~ line 41 ~ blobString", blobString)
    console.log("🚀 ~ file: generateStaffCSV.js ~ line 45 ~ argv.o", argv.o)
    fs.writeFileSync(argv.o, blobString, 'utf8');
} else {
    console.log(outData)
}
