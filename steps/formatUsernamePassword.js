load('libs/lodash.min.js');

const LIMIT = n || 10;

const query = () => db.getCollection('_User').find({
    "status": "ACTIVE",
    "tutorialProcess": "FINISHED",
    "role": "NAILIST",
    "CBStaffId": { $exists: false}
}).projection({ username: 1 })
.sort({_created_at: -1})
.limit(LIMIT)

let users = [];
let passed = 0;

print('id, username');
do {
    users = query().skip(passed).toArray();
    let bulk = db.getCollection('_User').initializeUnorderedBulkOp();
    users.forEach((u, i) => {
        print(`${u._id}, salon_nailist_${("000"+(passed + i)).slice(-4)}`);
        bulk.find({ _id: u._id }).update({ $set: { 
            username: `salon_nailist_${("000"+(passed + i)).slice(-4)}`,
            oldUsername: u.username,
            _hashed_password: '$2a$10$XSmBFsaQW2ZcoZxr0BaiW.BKHbnPD8m5Qc6WmwUE3c7DcO0fZ5HVG', //123456789
        }});
    });
    
    bulk.execute();
    passed += users.length;
    
} while (!_.isEmpty(users) && passed < LIMIT)