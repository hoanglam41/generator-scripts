const argv = require('minimist')(process.argv.slice(2));
const Utils = require('../../libs/Utils');
const csv = require('csvtojson');
fs = require('fs');

(async function () {
  let listStaff = await csv().fromFile('data/staff_info.csv');
  listStaff = argv.n && listStaff.slice(0, argv.n);
  const staff_info = listStaff.map((st) => {
    return {
      name: st.name,
      email: st.email,
      phoneNumber: st.phoneNumber,
      birthday: st.birthday,
      gender: st.gender,
      location: {
        postCode: st.postCode,
        prefecture: st.prefecture,
        cityOrTown: st.cityOrTown,
        address: st.address
      }
    }
  });

  const ids = [];

  for (const staff of staff_info) {
    try {
      const res = await Utils.axios.SB_instance.post('/api/v1/staff', {...staff});
      console.log('Successfully created staff!', res.data.data.id);
      ids.push({id: res.data.data.id});
    } catch (err) {
      console.log(err)
    }
  }

  const blobString = await Utils.makeCSV(ids, {id: "id"});
  await fs.writeFileSync('data/staff_ids.csv', blobString, 'utf8');

  console.log('step 3. DONE')
})();
