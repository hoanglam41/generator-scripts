const argv = require('minimist')(process.argv.slice(2));
const csv = require('csvtojson');
const _ = require('lodash');
fs = require('fs');
const Utils = require('../../libs/Utils');

let __SERVICES_IDS_FILE_NAME = 'Service_ids.csv'
let __SericesIdsData = [];

const createServiceInCategory = async ({id, __link_file, n}) => {
  if (!id || !__link_file || !n) {
    return;
  }

  let servicesList = await csv().fromFile(`data/${__link_file}`);
  servicesList = servicesList.slice(0, n)
  for (const service of servicesList) {
    const res = await Utils.axios.SB_instance.post('/api/v1/services', {
      "categoryId": id,
      "name": service.name,
      "duration": Number(service.duration),
      "price": Number(service.price),
      "description": service.description,
      "active": !!service.active
    });

    __SericesIdsData.push({
      "id": res.data.data.id,
      "categoryId": id,
    });
  }

  return;
};

(async function () {
  try {
    __SericesIdsData = await csv().fromFile(`data/${__SERVICES_IDS_FILE_NAME}`);
  } catch (err) {
    console.log('err');
    console.log('=> Create new file ' + __SERVICES_IDS_FILE_NAME);
  }

  const categorieIds = await csv().fromFile(argv.file || 'data/Category_ids.csv');
  console.log(categorieIds);

  // for (const category of categorieIds) {
  //   try {
  //     await createServiceInCategory(category);
  //   } catch (err) {
  //     console.log(err)
  //   }
  // }
  //
  // const blobString = await Utils.makeCSV(__SericesIdsData, {id: "id", categoryId: "categoryId"});
  // await fs.writeFileSync(`data/${__SERVICES_IDS_FILE_NAME}`, blobString, 'utf8');

  console.log('step 2. DONE')
})();
