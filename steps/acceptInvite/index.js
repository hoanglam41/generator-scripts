const argv = require('minimist')(process.argv.slice(2));
const csv = require('csvtojson');
const Utils = require('../../libs/Utils');

console.log(argv);

(async function () {
  let acceptList = await csv().fromFile('data/staff_invited.csv');
  acceptList = argv.n && acceptList.slice(0, argv.n);
  console.log(acceptList);

  for (const user of acceptList) {
    try {
      await Utils.axios.NL_instance.post('CB/test/responseJoinSalonInvitation', {
        sourceId: user.sourceId,
        testEnv: 'Aa123456@aA'
      })
    } catch (err) {
      console.log(err.response.data)
    }
  }

  console.log('step 6. DONE')
})();
