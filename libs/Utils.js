const path = require('path');
require('dotenv').config({
  path: path.resolve(process.cwd(), process.env.NODE_ENV ? `.env.${process.env.NODE_ENV}` : '.env')
});

const _ = require('lodash');
const axios = require('axios').default;
const curlirize = require('axios-curlirize');

const Utils = {};

Utils.genPhoneNum = () => {
    return "0" + Math.floor(100000000 + Math.random() * 900000000);
}

const CSVStruct = {
  "name": "name",
  "email": "email",
  "phoneNumber": "phoneNumber",
  "birthday": "birthday",
  "gender": "gender",
  "postCode": "postCode",
  "prefecture": "prefecture",
  "cityOrTown": "cityOrTown",
  "address": "address"
}
Utils.makeCSV = (dataList, struct = CSVStruct) => {
  const header = Object.keys(struct);
  let result = dataList.map(data => {
    let line = '';
    for (let keyPath in struct) {
      line += (_.get(data, struct[keyPath], '') + ',');
    }
    return line;
  });

  result.unshift(header);
  return result.join('\n');
}

Utils.axios = {
  NL_instance: axios.create({
    baseURL: process.env.NAILIE_API || 'https://nailie.scrum-dev.com',
    headers: {
      'Content-Type': 'application/json',
    },
  }),

  SB_instance: axios.create({
    baseURL: process.env.SALON_BOARD_API || 'https://server.nlsb-dev.scrum-dev.com',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': process.env.SALON_AUTH_TOKEN || `bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkFrdE9jTHJ4dFJVNHhKMHZLY1F2OSJ9.eyJodHRwczovL3VzZXJfbWV0YWRhdGEiOnsicGhvbmVfbnVtYmVyIjoiMDk4OTQ3ODQ2MjQiLCJidXNzaW5lc3NfbmFtZSI6Ik5BSUxJRSBERVYiLCJ2ZXJpZmllZCI6dHJ1ZSwibG9nZ2VkX2luIjpmYWxzZSwic2Fsb25faWQiOiIyNTEwNTQyOTcwMzU0NTgifSwiaHR0cHM6Ly9hdXRob3JpemF0aW9uIjp7InJvbGVzIjpbXX0sImh0dHBzOi8vZW1haWwiOiJ5ZW5ubGRldkB5b3BtYWlsLmNvbSIsImlzcyI6Imh0dHBzOi8vbmFpbGllLXNhbG9uYm9hcmQtaW50ZWdyYXRlLnVzLmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1ZmI1ZGU4NzFiZGEwMDAwNzVlMTFkMWYiLCJhdWQiOlsiaHR0cHM6Ly9uYWlsaWUtc2Fsb25ib2FyZC1pbnRlZ3JhdGUudXMuYXV0aDAuY29tL2FwaS92Mi8iLCJodHRwczovL25haWxpZS1zYWxvbmJvYXJkLWludGVncmF0ZS51cy5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNjA2MzczODc1LCJleHAiOjE2MDY0NTk4NzUsImF6cCI6IndsbUg5ZjczNXBkNDJKNWpGcVZ1cUdVcWg3OGVlMHhKIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCBvZmZsaW5lX2FjY2VzcyJ9.eAn7KuJm-f1Rvu86QLm-z4JkTIOu_ed5auTTzQYyRLAs3EA1B5PV-xIzBmQxY6gWglo8_aFJXuvicSZ8JA0kXUwDG_7bYnobYD4-88aKr6VPjrKQQ3Y0zQx3TqD4GZK44XF4O4UN2zctrIWocSo6MGU6ntXlY6iMQVnEZ6rU5r-S0RchcvbpXPTILLXbp2LvnedM45vRcyFpfv4v5-6i4MQLJ_KM7oFIcGIisH9zwlB9L98skVc_7d8LoWUgA8PpYq_2PID7oKqUrspohXmTVepG6LK2TipVdOI6Ss1uagvy8nl72I_pkOCj_Jk72T2FY7jE61UpvXpCnRG8S-1Liw`
    },
  }),
}

curlirize(Utils.axios.NL_instance);
curlirize(Utils.axios.SB_instance);

module.exports = Utils;