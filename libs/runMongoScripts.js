require('./Utils');
const { exec } = require('child_process');
const argv = require('minimist')(process.argv.slice(2));

exec(`mongo --eval 'var n=${argv.n}' "${process.env.NAILIE_DB_URL}" ${argv._[0]}`, (err, stdout, stderr) => {
  if (err) {
    console.log(`stderr: ${stderr}`);
  }

  // the *entire* stdout and stderr (buffered)
  console.log(`stdout: ${stdout}`);
});
