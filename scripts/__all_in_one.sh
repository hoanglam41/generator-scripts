### ENV CONFIG
#NODE_ENV=xxx (NODE_ENV=demo) before each node command or copy/paste override .env file

### OPTIONAL STEP
#Step 0
#0.a. Migrate username password of Nailie's nailists => save output BY HAND into `nailistStaff.csv`
#node libs/runMongoScripts.js -n 12 steps/formatUsernamePassword.js

#0.b. Generate booking
#node steps/generateBooking

### FLOW
#Step 1: Create categories **input: Category.csv (config number of each service here too) **ouput: Category_ids.csv
sudo node steps/st1_createCategory

#Step 2: Create services **input: Category_ids.csv, **output: Service_ids.csv
sudo node steps/st2_createService

#Step 3a: Gen staff info: clone staff.json into staff_info.csv
sudo node steps/generateStaffCSV.js -i data/staff.json -o data/staff_info.csv -n 100

#Step 3b: Create 10 staff: base on staff_info.csv, create n staffs, then save ids into staff_ids.csv
sudo node steps/create_staff -n 10

#Step 4: Assign service: assign all Step2.Service_ids.csv =>> Step.3.b.staff_ids.csv
sudo node steps/assign_service

#Step 5: Invite staff: base on nailistStaff.csv from Step.0.a **output: staff_invited.csv
sudo node steps/inviteStaff -n 1

#Step 6: Accept invite: base on staff_invited.csv from Step.5
sudo node steps/acceptInvite -n 1
